using Microsoft.AspNetCore.Mvc;

public class AuthController: Controller{
    private IAuthDatabaseService _AuthService;
    public AuthController(IAuthDatabaseService authService){
        _AuthService = authService;
    }
    public IActionResult AdminLogin(){
        return View("adminLogin");
    }
    
    [HttpPost]
    public IActionResult AdminLogin(UserModel credentials){
        _AuthService.Login(credentials);
          if(ModelState.IsValid){
            if(credentials.userId == 1234 && credentials.password == "admin"){
            return RedirectToAction(actionName: "Index", controllerName: "Admin");
        }else{
            return  RedirectToAction("AdminLogin");
        }}else{
            return View(credentials);
        }
    }
    public IActionResult UserLogin(){
        return View();
    }
    [HttpPost]
    public IActionResult UserLogin(UserModel credentials){
            if(ModelState.IsValid){
                if(credentials.userId == 1004 && credentials.password == "password"){
                    return RedirectToAction(actionName: "Index", controllerName: "User");
                }else{
                    return  RedirectToAction("AdminLogin");
                }
            }else{
            return View(credentials);
            }
    }
    public IActionResult Register(){
        return View();
    }
    public IActionResult Logout(){
       return RedirectToAction("Index", "Home");
    }

}
using System.Data.SqlClient;
using library.Models;

public class LibraryDataBaseService : ILibraryDataBaseService
{

    public IDatabaseService service  {get;set;}
    public SqlConnection con{get;set;}
    public LibraryDataBaseService(IDatabaseService service){
        this.service = service;
        this.con = service.getConnection();
    }
    public LibraryDataBaseService(){

    }
    public void lendBook(UserLendBook credentials)
    {
        bool decrease = true;
        try
        {
            con.Open();
            string dt = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");
            string insert = "INSERT INTO BookTransactions(userId,bookId,expiryDate) Values(@userid,@bookid,@expiryDate)";
            try
            {
                SqlCommand inst = new SqlCommand(insert, con);
                inst.Parameters.AddWithValue("@userid", credentials.userId);
                inst.Parameters.AddWithValue("@bookid", credentials.bookId);
                inst.Parameters.AddWithValue("@expiryDate", dt);
                inst.ExecuteNonQuery();
                try
                {
                    updateCopies(credentials.bookId,decrease);
                }
                catch (Exception e)
                {
                    Console.WriteLine("update not succesfull");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Insertion failed");
                Console.WriteLine(e);
            }
            con.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("connection failed");
            Console.WriteLine(e);
        }
    }


    private void updateCopies(int bookId, bool decrease)
    {
        try{
            string update_query = "";
            if(decrease){
                update_query = "UPDATE BooksAvailable SET copiesAvailable = copiesAvailable -1 WHERE bookId=@bookid";
            }
            else{
                update_query = "UPDATE BooksAvailable SET copiesAvailable = copiesAvailable +1 WHERE bookId=@bookid";

            }
                SqlCommand decreaseCount = new SqlCommand(update_query, con);
                decreaseCount.Parameters.AddWithValue("@bookid", bookId);
                decreaseCount.ExecuteNonQuery();
        }
        catch(Exception e){
            Console.WriteLine(e);
            throw new Exception();
        }
    }

    public void returnBook(int bookTransactionId)
    {
        bool decrease = false;
        try
        {
            con.Open();
            try{
                int bookid = -1;
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT bookId FROM BookTransactions where transactionId=@transactionid;", con);
                    cmd.Parameters.AddWithValue("@transactionid", bookTransactionId);
                    SqlDataReader bookidreader = cmd.ExecuteReader();
                    if (bookidreader.Read())
                    {
                        bookid = (int)bookidreader["bookId"];
                    }
                    bookidreader.Close();
                }
                catch (Exception newe)
                {
                    Console.WriteLine("cant get bookid");
                }
                string delquery = "DELETE FROM BookTransactions WHERE transactionId=@transactionid";
                SqlCommand delcmd = new SqlCommand(delquery, con);
                delcmd.Parameters.AddWithValue("@transactionid", bookTransactionId);
                int res = delcmd.ExecuteNonQuery();
                try
                {
                    updateCopies(bookid,decrease);
                }
                catch (Exception juste)
                {
                    Console.WriteLine("failed");
                }
            }
            catch (Exception aloe)
            {
                Console.WriteLine("deletion failed" + aloe);
            }
            con.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("connection failed");
        }
    }

    public List<BookModel> showBooks(DatabaseService dbService){
        List<BookModel> books = new List<BookModel>();
        try{
            string getAllBooks = "SELECT Books.bookId,Books.bookName,Books.bookAuthor,BooksAvailable.copiesAvailable FROM Books,BooksAvailable WHERE Books.bookId = BooksAvailable.bookId AND BooksAvailable.copiesAvailable >0 ";
            SqlCommand fetchBooks = new SqlCommand(getAllBooks,con);
            SqlDataReader booksReader = fetchBooks.ExecuteReader();
            while(booksReader.Read()){
                BookModel book = new BookModel((int)booksReader["bookId"],booksReader["bookName"].ToString(),booksReader["bookAuthor"].ToString(),(int)booksReader["copiesAvailable"]);
                books.Add(book);
            }
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return books;
    }

}
using System.Data.SqlClient;
using library.Models;

public class AdminDataBaseService : IAdminDataBaseServices {

    public SqlConnection? con;
    public IDatabaseService dbService;

    public AdminDataBaseService(IDatabaseService dbService){
        this.dbService = dbService;
        this.con = dbService.getConnection();
    }

    public void AddBook(BookModel newBook){
        try{
            con.Open();
            string addBookQuery = "INSERT INTO Books(bookName,bookAuthor,noOfCopies) OUTPUT INSERTED.bookId VALUES (@bookName,@bookAuthor,@noOfCopies)";
            SqlCommand insertBook = new SqlCommand(addBookQuery,con);
            insertBook.Parameters.AddWithValue("@bookName",newBook.bookName);
            insertBook.Parameters.AddWithValue("@bookAuthor",newBook.bookAuthor);
            insertBook.Parameters.AddWithValue("@noOfCopies",newBook.noOfCopies);
            try{
                int bookId = (int) insertBook.ExecuteScalar();
                SqlCommand addCopies = new SqlCommand("INSERT INTO BooksAvailable VALUES(@bookId,@copiesAvailable)",con);
                addCopies.Parameters.AddWithValue("@bookId",bookId);
                addCopies.Parameters.AddWithValue("@copiesAvailable",newBook.noOfCopies);
                try{
                    addCopies.ExecuteNonQuery();
                    Console.WriteLine("ADDED INTO BOOKS AVAILABLE");
                }
                catch(Exception addCopiesFailed){
                    Console.WriteLine("copies are not added");

                }
            }
            catch(Exception insertionFailed){
                Console.WriteLine("insertion failed");
            }
            con.Close();
        }
        catch(Exception e){
            Console.WriteLine("connection failed");
        }
    }

    public void DeleteBook(int bookId){

        try{
            con.Open();
            //string deleteBook = "DELETE FROM Books WHERE bookId = @bookId";
            string deleteBook = "DELETE FROM BooksAvailable WHERE bookId = @bookid";
            SqlCommand deleteBookCmd = new SqlCommand(deleteBook,con);
            deleteBookCmd.Parameters.AddWithValue("@bookid",bookId);
            try{
                int res = deleteBookCmd.ExecuteNonQuery();
                try{
                    SqlCommand deleteCopies = new SqlCommand("DELETE FROM Books WHERE bookId = @bookId",con);
                    deleteCopies.Parameters.AddWithValue("@bookId",bookId);
                    int confirmation = deleteCopies.ExecuteNonQuery();
                    Console.WriteLine(confirmation);
                }
                catch(Exception deleteCopies){
                    Console.WriteLine(deleteCopies);
                }
            }
            catch(Exception deleteExecption){
                System.Console.WriteLine(deleteExecption);
                Console.WriteLine("delete exception");
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
    }

    public List<BookModel> GetAllBooks(){
        List<BookModel> allBooks = new List<BookModel>();
        try{
            con.Open();
            string getAllBooksQuery = "SELECT * FROM Books";
            SqlCommand getAllBookscmd = new SqlCommand(getAllBooksQuery,con);
            try{
                SqlDataReader getAllBooksReader = getAllBookscmd.ExecuteReader();
                while(getAllBooksReader.Read()){
                    BookModel book = new BookModel((int)getAllBooksReader["bookId"],getAllBooksReader["bookName"].ToString(),getAllBooksReader["bookAuthor"].ToString(),(int)getAllBooksReader["noOfCopies"]);
                    allBooks.Add(book);
                }
            }
            catch(Exception dataReaderFailed){
                Console.WriteLine("data is not fetched");
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return allBooks;
    }


}


using System.Data.SqlClient;

public class AuthDatabaseService : IAuthDatabaseService{

    public IDatabaseService dbService;
    private SqlConnection con;

    public AuthDatabaseService(IDatabaseService dbService){
        this.dbService = dbService;
        this.con = dbService.getConnection();
    }

    
    public bool Login(UserModel user){
        try{
            con.Open();
            try{
                string findUserQuery = "SELECT userId from Users WHERE userId = @userid and password=@password";
                SqlCommand findUsercmd = new SqlCommand(findUserQuery,con);
                findUsercmd.Parameters.AddWithValue("@userid",user.userId);
                findUsercmd.Parameters.AddWithValue("@password",user.password);
                SqlDataReader userReader = findUsercmd.ExecuteReader();
                if(userReader.Read()){
                    return true;
                }
                else{
                    return false;
                }
            }
            catch(Exception findUserException){
                throw;
            }
            con.Close();
        }
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return false;
    }
    public int Register(UserModel user){
        try{
            con.Open();
            int userId = -1;
            SqlCommand registeruser = new SqlCommand("INSERT INTO Users(userName,password) VALUES(@username,@password) SELECT SCOPE_IDENTITY()",con);
            registeruser.Parameters.AddWithValue("@username",user.userName);
            registeruser.Parameters.AddWithValue("@password",user.password);
            try{
                userId =  Convert.ToInt32(registeruser.ExecuteScalar());
            }
            catch(Exception registerError){
                Console.WriteLine("register error");
                throw new Exception();
            }
            con.Close();
            return userId;
        }   
        catch(Exception connectionFailed){
            Console.WriteLine("connection failed");
        }
        return 0;
    }
}
public class UserModel{
    public int userId {get;set;}
    public string userName {get;set;}

    public string password {get;set;}

    /*used for login */
    public UserModel(int userId,string password)
    {
        this.userId = userId;
        this.password = password;
    }

    /*used for register*/
    public UserModel(string userName,string password){
        this.userName = userName;
        this.password = password;
    }

}
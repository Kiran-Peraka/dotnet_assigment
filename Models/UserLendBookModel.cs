namespace library.Models;

public class UserLendBook{
    public int userId {get;set;}
    public int bookId {get;set;}

    public UserLendBook(int userid,int bookid){
        this.userId = userid;
        this.bookId = bookid;
    }
    public UserLendBook(){
        
    }

}


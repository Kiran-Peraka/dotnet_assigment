using library.Models;
using System.Data.SqlClient;
public interface IAdminDataBaseServices {
    public void AddBook(BookModel book);
    public void DeleteBook(int id);

    public List<BookModel> GetAllBooks();
}


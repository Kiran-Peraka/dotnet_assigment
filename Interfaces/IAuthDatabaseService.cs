using System.Data.SqlClient;
using library.Models;

public interface IAuthDatabaseService {
   bool Login(UserModel user);
 int Register(UserModel user);
}
using library.Models;
using System.Data.SqlClient;

public interface ILibraryDataBaseService{
    void lendBook(UserLendBook credentials);
      void returnBook (int bookTransactionId);
}